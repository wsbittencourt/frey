Tickets = new Meteor.Collection('tickets');

function formatDate(date) {
    var monthNames = [
        "Jeneiro", "Fevereiro", "Março",
        "Abril", "Maio", "Junho", "Julho",
        "Agosto", "Setembro", "Outubro",
        "Novembro", "Dezembro"
    ];
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var min = date.getMinutes();
    var hour = date.getHours();
    if (min < 10) {
        min = '0' + min;
    }
    if (hour < 10) {
        hour = '0' + hour;
    }

    return '['+hour + ":" + min + "] " + day + ' de ' + monthNames[monthIndex] + ', ' + year;
}

TicketsSchema = new SimpleSchema({
    usuario: {
        type: String,
        autoValue: function () {
            if (this.isInsert) {
                return Meteor.user().profile.nome
            }
        }
    },
    autor: { type: String },
    cliente: {
        type: String
    },
    tipo: {
        type: String
    },
    titulo: { type: String },
    texto: { type: String },
    createAt: {
        type: String,
        autoValue: function () {
            /*if (this.isInsert) {
                function formatDate(date) {
                    var day = date.getDate();
                    var month = date.getMonth(); //JS conta o mês a partir do 0.
                    month = month + 1;
                    var year = date.getFullYear();
                    if (day < 10) {
                        day = '0' + day;
                    }
                    if (month < 10) {
                        month = '0' + month;
                    }
                
                    return day + '-' + month + '-' + year;
                }
                return formatDate(new Date());
            }*/
            return new Date();
        }
    },
    timer: {
        type: Date,
        autoValue: function() {
            let t = new Date();
            t.setHours(t.getHours() + 4);
            return t;
        }
    },
    criado: {
        type: String,
        autoValue: function () {
            if (this.isInsert) {
                return formatDate(new Date());
            }
        }
    },
    setor: {
        type: String
    },
    nOS: {
        type: String,
        autoValue: function () {
            if (this.isInsert) {
                return Meteor.call('nos') + 1
            }
        }
    },
    status: {
        type: String,
        autoValue: function () {
            if (this.isInsert) {
                return 'novo'
            }
        }
    },
    responsavel: {
        type: String
    },
    open: {
        type: Boolean
    },
    comentarios: {
        type: [{}],
        optional: true,
        blackbox: true
    }
});

Tickets.attachSchema(TicketsSchema);

Meteor.methods({

    'addTicket': function (nT) {
        if (this.userId) {
            Tickets.insert(nT, { validate: false });
        } else {
            console.log('Error: Usuário não logado no sistema.')
        }
    },
    'rmChamado': function (id) {
        Tickets.remove(id);
    },

    'updateTicket': function (id, commit) {
        let ticket = Tickets.findOne(id);
        let usr = commit.usuario;
        if (ticket.responsavel === "") {
            Tickets.update({ _id: ticket._id },
                { $set: { status: 'atribuido', responsavel: usr } }, { validate: false });

            let init = {
                usuario: usr,
                texto: usr + ' moveu o chamado para si.',
                hora: commit.hora
            }
            Tickets.update({ _id: ticket._id },
                { $push: { comentarios: init } }, { validate: false })
        }

        Tickets.update({ _id: ticket._id }, { $push: { comentarios: commit } }, { validate: false })
    },

    'closeTicket': function (id) {
        let ticket = Tickets.findOne(id);
        Tickets.update({ _id: ticket._id },
            { $set: {status: 'fechado', open: false } }, { validate: false });
    },

    'moveTicket': function (id, nSetor, nUsuario, text) {
        let ticket = Tickets.findOne(id);
        let usr = Meteor.user().profile.nome;

        let colab = Accounts.findUserByUsername(nUsuario);

        let mv = {
            usuario: usr,
            texto: usr + ' moveu o chamado para ' + colab.profile.nome + '.',
            hora: formatDate(new Date())
        }

        if (text !== '<p><br></p>') {
            mv.texto = mv.texto + "<br>" + text;
        }

        Tickets.update({ _id: ticket._id },
            { $push: { comentarios: mv } }, { validate: false });

        Tickets.update({ _id: ticket._id },
            { $set: { setor: nSetor, responsavel: colab.profile.nome, status: 'atribuido' } }, { validate: false });
    },

    'alterar': function (id, s) {
        //var ticket = Tickets.findOne(id);
        Tickets.update({ _id: id/*ticket._id*/ },
            { $set: { status: s } }, { validate: false });
    },

    'reopen': function (id) {
        let usr = Meteor.user().profile.nome;

        let mv = {
            usuario: usr,
            texto: usr + ' reabriu o chamado.',
            hora: formatDate(new Date())
        }

        Tickets.update({_id: id},
            { $push: { comentarios: mv } }, { validate: false });
        Tickets.update({_id: id}, { $set: { status: 'atribuido',open: true} }, { validate: false });
    },

    'agenda': function(id, name, sec){
        Tickets.update({ _id: id/*ticket._id*/ },
            { $set: { responsavel: name, setor: sec} }, { validate: false });   
    }
});