Clientes = new Meteor.Collection('clientes');

Meteor.methods({
    'addClient': function (client, uname) {
        Clientes.insert(client);
    },
    'rmClient': function (id) {
        Clientes.remove(id);
    },

    'updateClient': function (up, idCliente) {
        Clientes.update(idCliente, {
            $set: {
                nomeFantasia: up.nomeFantasia,
                responsavel: up.responsavel,
                email: up.email,
                telefone_01: up.telefone_01,
                tipo: up.tipo,
                ativo: up.ativo
            },
        });
    },

    'getClient': function(cid) {
        return Clientes.findOne({nomeFantasia : cid});
    }

});