UProfile = new Meteor.Collection('uprofile');

Meteor.methods({
    'accProfile': function (uname) {
        return UProfile.findOne({
            'username': uname
        });
    },
    'usrPorSetor': function () {
        try{
            let atual = UProfile.findOne({
                'username': Meteor.user().username
            });

            let profs = UProfile.find({setor: atual.setor});
            

            let names = [];
            profs.forEach(function (row) {
                let name = Meteor.users.findOne({username: row.username});
                name = name.profile.nome;
                names.push({uname: name});            
            });
            return names;
        }catch(er){
            return "Erro ao acessar o setor do usuário!";
        }
    },

    'usrList': function (oSetor) {
        try{
            let profs = UProfile.find({setor: oSetor});
            let names = [];
            profs.forEach(function (row) {
                let name = Meteor.users.findOne({username: row.username});
                let usr = name.username;
                name = name.profile.nome;
                names.push({nick: name, username: usr});            
            });
            return names;
        }catch(er){
            return "Erro ao acessar o setor do usuário!";
        }
    }

});
