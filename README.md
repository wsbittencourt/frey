Pacotes utilizados (meteor list)

```
accounts-password      1.4.0  Password support for accounts
accounts-ui            1.1.9  Simple templates to add login widgets to an app
blaze-html-templates   1.1.2  Compile HTML templates into reactive UI with Meteor Blaze
ecmascript             0.8.2  Compiler plugin that supports ES2015+ in all .js files
es5-shim               4.6.15  Shims and polyfills to improve ECMAScript 5 support
iron:router            1.1.2  Routing specifically designed for Meteor
meteor-base            1.1.0  Packages that every Meteor app needs
mobile-experience      1.0.4  Packages for a great mobile user experience
mongo                  1.2.2  Adaptor for using MongoDB and Minimongo over DDP
reactive-var           1.0.11  Reactive variable
shell-server           0.2.4  Server-side component of the `meteor shell` command.
standard-minifier-css  1.3.5  Standard css minifier used with Meteor apps by default.
standard-minifier-js   2.1.1  Standard javascript minifiers used with Meteor apps by default.
tracker                1.1.3  Dependency tracker to allow reactive callbacks
twbs:bootstrap         3.3.6  The most popular front-end framework for developing responsive, mobile first proje...
```