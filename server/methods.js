Meteor.methods({

    //Meteor.userId();
    'isAdmin': function (uName) {
        let prof = UProfile.findOne({
            username: uName
        });
        return prof.setor === 'admin';
    },

    'filtroCliente': function (nome, ty, at) {
        return Clientes.find({}, {
            fields: {
                nomeFantasia: nome
            }
        });
    },

    nos: function () {
        return Tickets.find().count();
    },

    setor: function () {
        let prof = UProfile.findOne({
            username: Meteor.user().username
        });
        console.log('Setor eh: ' + prof.setor)
        return prof.setor;
    },

    serverTime: function () {
        return Date.parse(new Date());
    },

    novoCliente: function (usr) {
        Accounts.createUser({
            username: usr.username,
            password: usr.password,
            email: usr.email,
            profile: {
                nome: usr.nome,
            }
        });

        UProfile.insert({
            username: usr.username,
            tipo: 'client',
            setor: 'none',
            empresa: usr.empresa
        });
    },

    novoColaborador: function (usr) {
        Accounts.createUser({
            username: usr.username,
            password: usr.password,
            email: usr.email,
            profile: {
                nome: usr.nome,
            }
        });

        let type = 'user';
        if (usr.admin) {
            type = 'root';
        }
        UProfile.insert({
            username: usr.username,
            tipo: usr.tipo,
            setor: usr.setor
        });
    },

    resetPasswd: (admid, uname) => {
        let prof = UProfile.findOne({
            username: admid
        });
        if (prof.setor === 'admin') {
            let npw = 'lN321';
            let user = Meteor.users.findOne({ username: uname });
            Accounts.setPassword(user._id, npw);
        } else {
            throw new Meteor.Error(666, 'Erro 666: Permissão negada', 
            'Usuário não tem privilégios necessários.');
        }
    },

    changes: (admId,uname, obj) => {
        let prof = UProfile.findOne({
            username: admId
        });
        if (prof.setor === 'admin') {
            Meteor.users.update({ username: uname }, {
                $set:{
                    username: obj.username,
                    'emails.0.address' : obj.email,
                    'profile.nome' : obj.nome,                    
                }
            });
            UProfile.update({ username: uname }, {
                $set:{
                    username: obj.username                 
                }
            });
            
            if(typeof(obj.empresa) !== "undefined"){
                UProfile.update({ username: uname }, {
                    $set:{
                        empresa: obj.empresa                  
                    }
                });
            }else if(typeof(obj.setor) !== "undefined"){
                UProfile.update({ username: uname }, {
                    $set:{
                        setor: obj.setor                  
                    }
                });
            }

        } else {
            throw new Meteor.Error(666, 'Erro 666: Permissão negada', 
            'Usuário não tem privilégios necessários.');
        }
    },

    removeClient: (admId, id, uname) =>{
        let prof = UProfile.findOne({
            username: admId
        });
        if (prof.setor === 'admin') {
            UProfile.remove({username:uname});
            Meteor.users.remove(id);
        } else {
            throw new Meteor.Error(666, 'Erro 666: Permissão negada', 
            'Usuário não tem privilégios necessários.');
        }
    }
});