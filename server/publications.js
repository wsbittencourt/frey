Meteor.publish('clientes',function(limit){
    return Clientes.find({},{limit:limit, sort: {$natural: -1}});
});

Meteor.publish('searchClient',function(){
    return Clientes.find({},{sort: {$natural: -1}});
});

Meteor.publish('chamados',function(){
    return Tickets.find({},{sort: {$natural: -1}});
});

Meteor.publish('client', (cid) => {
    return Clientes.find({ nomeFantasia: cid });
});

Meteor.publish('allUsers', function() {
    return Meteor.users.find();
})