Alerts = new Mongo.Collection(null);

newAlert = function (message) {
    Alerts.insert({
        message: message
    });
};

Template.alerts.helpers({
    alerts: function () {
        return Alerts.find();
    }
});
