
Template.listaChamados.onCreated(function () {
    Meteor.subscribe('chamados');
});


Template.listaChamados.helpers({
    chamados: function () {
        try{
            var sector = Session.get('perfil').setor;
            let categoria = Session.get('list');
            if(sector == 'admin') {
                switch (categoria){
                    case 'emAberto':
                        return Tickets.find({open: true});
                    case 'fechados':
                        return Tickets.find({open: false});
                    default: //todos
                        return Tickets.find();
                }
            }else{
                switch (categoria){
                    case 'emAberto':
                        return Tickets.find({open: true, setor: sector});
                    case 'fechados':
                        return Tickets.find({open: false, setor: sector});
                    default: //todos
                        return Tickets.find({setor: sector});
                }                
            }
        }catch (err){//Faz nada
        }
    },

    perfil: function () {
        return Session.get('perfil');
    },

    isAdm: function (setor) {
        return setor === 'admin';
    }
});

Template.listaChamados.events({
    
});