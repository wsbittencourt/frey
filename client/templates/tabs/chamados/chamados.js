Session.setDefault('filtroChamados', false);

function refresh() {
    $('#filtroClientes').val('');
    $('#data').val('');
    $("#filtroSetor").val('');
    $("#pessoa").val('');
    $("#filtroTitulo").val('');
    $("#filtroTipo").val('');

    Session.set('ticketSearch',
        {
            tipo: null,
            titulo: null,
            pessoa: null,
            createAt: null,
            setor: null,
            cliente: null
        });
}

Template.chamados.onCreated(function () {
    Session.set('nos', '');
    Session.set('list', 'emAberto');

    this.colegas = new ReactiveVar([]);
    refresh();
});

Template.chamados.rendered = function () {
    $('#listaClientes').selectpicker();
    $(document).ready(function () {
        $('#corpoChamado').summernote({
            placeholder: 'Adicione uma descrição do problema',
            tabsize: 4,
            height: 200,
        });
    });

    this.$('.datepicker').datepicker({
        format: 'dd-mm-yyyy'
    });

    const instance = Template.instance();

    Meteor.call('usrPorSetor', function (error, result) {
        instance.colegas.set(result);
    });
};

var renderTimeout = false;
Template.itemSelect.rendered = function () {
    if (renderTimeout !== false) {
        Meteor.clearTimeout(renderTimeout);
    }
    renderTimeout = Meteor.setTimeout(function () {
        $('#listaClientes').selectpicker("refresh");
        renderTimeout = false;
    }, 10);
};

Template.chamados.helpers({
    todosClientes: function () {
        Meteor.subscribe('searchClient');
        var query = Clientes.find({ ativo: 'Sim' });
        //Meteor.subscribeWithPagination('clientes',7);
        return query;
    },
    statusFiltro: function () {
        return Session.get('filtroTicket');
    },
    statusPenteFino: function () {
        return Session.get('penteFino');
    },
    filtro: function () {
        let numOS = Session.get('nos');
        let sector = Session.get('perfil').setor;
        let categoria = Session.get('list');

        if (numOS === '' || numOS === 0) {
            if (sector == 'admin') {
                switch (categoria) {
                    case 'emAberto':
                        return Tickets.find({ open: true });
                    case 'fechados':
                        return Tickets.find({ open: false });
                    default: //todos
                        return Tickets.find();
                }
            } else {
                switch (categoria) {
                    case 'emAberto':
                        return Tickets.find({ open: true, setor: sector });
                    case 'fechados':
                        return Tickets.find({ open: false, setor: sector });
                    default: //todos
                        return Tickets.find({ setor: sector });
                }
            }
        } else {
            //return Tickets.find({nOS: {$regex : ".*"+numOS+".*"}}); 
            if (sector == 'admin') {
                switch (categoria) {
                    case 'emAberto':
                        return Tickets.find({ open: true, nOS: numOS });
                    case 'fechados':
                        return Tickets.find({ open: false, nOS: numOS });
                    default: //todos
                        return Tickets.find({ nOS: numOS });
                }
            } else {
                switch (categoria) {
                    case 'emAberto':
                        return Tickets.find({ open: true, setor: sector, nOS: numOS });
                    case 'fechados':
                        return Tickets.find({ open: false, setor: sector, nOS: numOS });
                    default: //todos
                        return Tickets.find({ setor: sector, nOS: numOS });
                }
            }
        }
    },
    sector: function () {
        try {
            var sec = Session.get('perfil').setor;
            switch (sec) {
                case 'admin':
                    return 'ADMINISTRATIVO';
                case 'lab':
                    return 'LABORATÓRIO';
                case 'remoto':
                    return 'REMOTO';
                case 'externo':
                    return 'EXTERNO';
            }
        }
        catch (err) {/*Faz nada*/ }

    },
    usrs: function () {
        return Template.instance().colegas.get();
    },
    isAdm: function () {
        try {
            if (Session.get('perfil').setor === 'admin') {
                return true;
            } else {
                return false;
            }
        } catch (er) { }
    },
    opSetor: function () {
        try {
            var sec = Session.get('perfil').setor;
            switch (sec) {
                case 'lab':
                    return '<option value="lab">Laboratório</option>';
                case 'remoto':
                    return '<option value="remoto">Remoto</option>';
                case 'externo':
                    return '<option value="externo">Externo</option>';
            }
        }
        catch (err) {/*Faz nada*/ }
    },
    penteFino: function () {
        let f = Session.get('ticketSearch');
        let sector = Session.get('perfil').setor;
        let categoria = Session.get('list');

        switch (categoria) {
            case 'emAberto':
                if (f.tipo !== null && f.titulo !== null && f.createAt !== null
                    && f.setor !== null && f.cliente !== null) {
                    return Tickets.find({
                        open: true, tipo: f.tipo, titulo: f.titulo, usuario: f.titulo,
                        createAt: f.createAt, setor: f.setor, cliente: f.cliente
                    });
                } else {
                    return buscar('emAberto', f);
                }

            case 'fechados':
                if (f.tipo !== null && f.titulo !== null && f.createAt !== null
                    && f.setor !== null && f.cliente !== null) {
                    return Tickets.find({
                        open: false, tipo: f.tipo, titulo: f.titulo, usuario: f.titulo,
                        createAt: f.createAt, setor: f.setor, cliente: f.cliente
                    });
                } else {
                    return buscar('fechados', f);
                }
            case 'todos':
                if (f.tipo !== null && f.titulo !== null && f.createAt !== null
                    && f.setor !== null && f.cliente !== null) {
                    return Tickets.find({
                        tipo: f.tipo, titulo: f.titulo, usuario: f.titulo,
                        createAt: f.createAt, setor: f.setor, cliente: f.cliente
                    });
                } else {
                    return buscar('todos', f);
                }
        }
    }
});

Template.chamados.events({
    'change #filtroTicket': function () {
        if (document.getElementById('filtroTicket').checked) {
            Session.set('filtroTicket', true);
            $('#penteFino').removeAttr('checked');
            Session.set('penteFino', false);

        } else {
            Session.set('filtroTicket', false);
        }

    },
    'change #penteFino': function () {
        if (document.getElementById('penteFino').checked) {
            Session.set('penteFino', true);
            $('#filtroTicket').removeAttr('checked');
            Session.set('filtroTicket', false);
        } else {
            Session.set('penteFino', false);
        }
    },

    'change #listaClientes': function (event, template) {
        var id = $(event.currentTarget).val();
        Session.set('selectedClienteId', id);
        //console.log("ID Cliente : " + id);
        // additional code to do what you want with the category
    },

    'click #newTicket': function () {
        if ($('#listaClientes').val() === '' || $('#corpoChamado').summernote('code') === '<p><br></p>' ||
            $('#titulo').val() === '' || $('#autor').val() === '') {
            swal(
                'Erro ao abrir chamado',
                'Preencha todos os campos para prosseguir!',
                'error'
            );
        } else {
            var cId = $('#listaClientes').val();
            Meteor.subscribe('searchClient');
            var objCliente = Clientes.findOne({ nomeFantasia: cId });
            var nTicket = {
                texto: $('#corpoChamado').summernote('code'),
                autor: $('#autor').val().toUpperCase(),
                cliente: objCliente.nomeFantasia,
                titulo: $('#titulo').val().toUpperCase(),
                createAt: '',
                setor: Session.get('perfil').setor,
                nOS: '',
                status: '',
                usuario: '',
                criado: '',
                tipo: objCliente.tipo,
                responsavel: '',
                open: true,
                comentarios: new Array()
            }
            Meteor.call('addTicket', nTicket);
            $('#nChamado').modal('hide');

            //atualiza página
            document.location.reload(true);

            Bert.alert('Chamado aberto com sucesso!', 'success', 'growl-top-right');
            //$('#listaClientes').val('');
            $('#corpoChamado').summernote('code', '<p><br></p>');
            $('#titulo').val('');
            $('#autor').val('');
        }
    },
    'keyup #numOS, mouseup #numOS': function (event) {
        Session.set('nos', Number(event.target.value));
    },
    'click #emAberto': function () {
        Session.set('list', 'emAberto');
    },
    'click #todos': function () {
        Session.set('list', 'todos');
    },
    'click #fechados': function () {
        Session.set('list', 'fechados');
    },
    'click #btnBuscar': function () {
        let sector = Session.get('perfil').setor;
        let categoria = Session.get('list');
        let fcliente = $("#filtroClientes").val();
        let fdata = $("#data").val();
        let filtroSetor = $("#filtroSetor").val();
        let fpessoa = $("#pessoa").val();
        let ftitulo = $("#filtroTitulo").val().toUpperCase();
        let ftipo = $("#filtroTipo").val();

        if (ftipo === '') {
            ftipo = null
        }
        if (ftitulo === '') {
            ftitulo = null
        }
        if (fpessoa === '') {
            fpessoa = null
        }
        if (filtroSetor === '') {
            filtroSetor = null
        }
        if (fdata === '') {
            fdata = null
        }
        if (fcliente === '') {
            fcliente = null
        }

        Session.set('ticketSearch',
            {
                tipo: ftipo,
                titulo: ftitulo,
                pessoa: fpessoa,
                createAt: fdata,
                setor: filtroSetor,
                cliente: fcliente
            }
        )

        // Tickets.find({$or:[{setor:'admin'},{cliente: v}]})
    },
    'click #btnClear': function () {
        //Limpar campos do formulário

        refresh();
    },

    'click #btnFiltrar': () => {
        if (Session.get('filtroTicket') === true) {
            $('#filtroTicket').prop('checked', true);
        }else if(Session.get('penteFino') === true){
            $('#penteFino').prop('checked', true);
        }
    }

});