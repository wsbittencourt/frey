Template.moverChamado.onCreated(function () {
    this.setor = new ReactiveVar('');
    this.colaborador = new ReactiveVar('');
    this.grupo = new ReactiveVar('');
});

Template.moverChamado.rendered = function () {
    $(document).ready(function () {
        $('#textoMover').summernote({
            placeholder: 'Adicione um comentário.',
            tabsize: 4,
            height: 90,
        });
    });
};


Template.moverChamado.helpers({
    modalChamado: function () {
        return getTicket();
    },

    todosUsuarios: function () {
        return Template.instance().grupo.get();
    },
    sectorIsNotNull: function () {
        let sector = Template.instance().setor.get();
        if (sector !== "") {
            return true;
        } else { return false }
    },

    isAtt: function (status) {
        if (status === 'atribuido') {
            return true;
        } else { return false }
    },

    getStatus: function (status) {
        switch (status) {
            case "novo":
                return '<span class="chamado-novo glyphicon glyphicon-flag" title="Novo Chamado" aria-hidden="true"></span>';
            case "atribuido":
                return '<span class="chamado-atribuido glyphicon glyphicon-flag" title="Chamado em andamento!" aria-hidden="true"></span>';
            case 'fechado': //fechado
                return '<span class="chamado-fechado glyphicon glyphicon-flag" title="Chamado fechado" aria-hidden="true"></span>';
            case 'agendado': //fechado
                return '<span class="chamado-agendado glyphicon glyphicon-flag" title="Chamado fechado" aria-hidden="true"></span>';
        }
    },
});

Template.moverChamado.events({
    'change #mvSetor': function (event, template) {
        if ($('#mvSetor').val() === '') { template.setor.set(''); }
        else if ($('#mvSetor').val() === 'admin') { template.setor.set('admin'); }
        else if ($('#mvSetor').val() === 'lab') { template.setor.set('lab'); }
        else if ($('#mvSetor').val() === 'remoto') { template.setor.set('remoto'); }
        else if ($('#mvSetor').val() === 'externo') { template.setor.set('externo'); }

        if ($('#mvSetor').val() !== '') {
            let setor = Template.instance().setor.get();
            const instance = Template.instance();
            Meteor.call('usrList', setor, (error, result) => {
                instance.grupo.set(result);
            });
        }
        $('#mvUsr').val('');
    },
    'change #mvUsr': function (event, template) {
        template.colaborador.set($('#mvUsr').val());
    },

    'click #sendTo': function (evt) {
        // Usar $('#mvUsr').val() para obter o valor corretor do campo usuário
        // caso contrário pode ocorrer bugs

        evt.preventDefault();
        var id = this._id;
        let colab = $('#mvUsr').val();
        let sec = $('#mvSetor').val();
        if (colab === '' || sec === '') {
            swal(
                'Erro',
                'Preencha os campos obrigatórios para finalizar a operação!',
                'error'
            );
        } else {
            swal({
                text: '',
                title: '<div class="cor">Deseja mover o chamado?</div>',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Sim, mover',
                cancelButtonText: 'Não, manter status',
            }).then(function () {
                var text = $('#textoMover').summernote('code');
                if (text === '<p><br></p>') {
                    swal({
                        title: '<div class="cor">Aviso<div class="cor">',
                        text: 'Deixar comentário em branco?',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Sim',
                        cancelButtonText: 'Não',
                    }).then(function () {
                        Meteor.call('moveTicket', Session.get('selectedTicketId'), sec, colab, text);
                        $('#mvTicket').modal('hide');
                        swal(
                            '',
                            'O chamado foi movido!',
                            'success'
                        );
                    })
                } else {
                    Meteor.call('moveTicket', Session.get('selectedTicketId'), sec, colab, text);
                    $('#textoMover').summernote('code', '<p><br></p>');
                    $('#mvTicket').modal('hide');
                    swal(
                        '',
                        'O chamado foi movido!',
                        'success'
                    );
                }
            }, function (dismiss) {
                swal(
                    '',
                    'O chamado não foi movido!',
                    'info'
                )
            });
        }
    }
});