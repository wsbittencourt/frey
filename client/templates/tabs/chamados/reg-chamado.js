Template.regChamado.helpers({
    perfil: function () {
        return Session.get('perfil');
    },

    isAdm: function (setor) {
        return setor === 'admin';
    },

    getStatus: function(status) {
        switch (status) {
            case "novo" : 
                return '<span class="chamado-novo glyphicon glyphicon-flag" title="Novo Chamado" aria-hidden="true" style="font-size: 20px;"></span>';
            case "atribuido" : 
                return '<span class="chamado-atribuido glyphicon glyphicon-flag" title="Chamado em andamento!" aria-hidden="true" style="font-size: 20px;"></span>';
            case 'fechado' : //fechado
                return '<span class="chamado-fechado glyphicon glyphicon-flag" title="Chamado fechado" aria-hidden="true" style="font-size: 20px;"></span>';
            case 'agendado' : //fechado
                return '<span class="chamado-agendado glyphicon glyphicon-flag" title="Chamado agendado" aria-hidden="true" style="font-size: 20px;"></span>';
        }
    }
});

Template.regChamado.events({
    'click #edit': function (e) {
        e.preventDefault();
        ModalHelper.editarChamado(this._id);
    },

    'click #excluir': function (e) {
        var id = this._id
        
        swal({
          text: '',
          title: '<div class="cor">Deseja excluir o chamado?</div>',
          type: 'question',
          showCancelButton: true,
          confirmButtonText: 'Sim, excluir',
          cancelButtonText: 'Não, manter',
        }).then(function() {
          Meteor.call('rmChamado', id, function (error, result) {
                if (error) {
                    alert(error);
                }
            });
            Bert.alert( 'Sucesso, chamado excluído do sistema!', 'success', 'growl-top-right' );
        }, function(dismiss) {
            Bert.alert( 'Chamado mantido no sistema!', 'info', 'growl-top-right' );            
        }); 
    },

    'click #transferir': function (e) {
        e.preventDefault();
        ModalHelper.moverChamado(this._id);
    },

    'click #reabrir': function() {
        var id = this._id
        
        swal({
          title: '',
          text: 'Deseja reabrir o chamado?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Sim, reabrir',
          cancelButtonText: 'Não, manter fechado',
        }).then(function() {
          Meteor.call('reopen', id, function (error, result) {
                if (error) {
                    alert(error);
                }
            });
            Bert.alert( 'Sucesso, chamado reaberto!', 'success', 'growl-top-right' );
        }, function(dismiss) {
            Bert.alert( 'Chamado continua fechado!', 'info', 'growl-top-right' );            
        }); 
    }

});


