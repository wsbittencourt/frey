Template.contador.onCreated(function () {
  this.timeinterval;
  this.agora = new ReactiveVar();
  this.endTime = new ReactiveVar();
  this.myTime = new ReactiveVar();

  const instance = Template.instance();
  this.timeinterval = Meteor.setInterval(function () {
    var srvTime;
    /*Meteor.call("getCurrentTime", function (error, result) { 
      if (error) {
        console.log(error);
      } else {
        srvTime = result;
      }           
    });*/
    srvTime = Date.parse(new Date());
    let t = Date.parse(instance.endTime.get()) - srvTime;
    let seconds = ("0" + Math.floor((t / 1000) % 60)).slice(-2);
    let minutes = ("0" + Math.floor((t / 1000 / 60) % 60)).slice(-2);
    let hours = ("0" + Math.floor((t / (1000 * 60 * 60)) % 24)).slice(-2);
    let days = Math.floor(t / (1000 * 60 * 60 * 24));

    instance.agora.set({
      'total': t,
      'days': days,
      'hours': hours,
      'minutes': minutes,
      'seconds': seconds
    });
    if (t <= 0) {
      Meteor.clearInterval(instance.timeinterval);
    }
    instance.myTime.set(t);

  }, 1000);
});

Template.contador.helpers({
  timer: function (eT) {
    try {
      Template.instance().endTime.set(eT);
      let tempo = Template.instance().myTime.get();
      let res = Template.instance().agora.get();
      if (tempo <= 0) {
        //timeinterval
        return "<span class='redTimer' title='Excedido o prazo de atendimento'>TEMPO EXCEDIDO</span>";
      }
      else if ((res.hours <= 3 && res.minutes <= 29) || res.hours <= 2) {
        return "<span class='yellowTimer' title='Tempo para atendimento'>" +
          res.hours + ':' + res.minutes + ':' + res.seconds + "</span>"
      } else {
        return "<span class='greenTimer' title='Tempo para atendimento'>" +
          res.hours + ':' + res.minutes + ':' + res.seconds + "</span>"
      }
    } catch (e) {
      //Faz nada
    }
  }
});