getTicket = function () {
    var tId = Session.get('selectedTicketId');
    var t = Tickets.findOne(tId);
    return t;
}

formatDate = function (date) {
    var monthNames = [
        "Jeneiro", "Fevereiro", "Março",
        "Abril", "Maio", "Junho", "Julho",
        "Agosto", "Setembro", "Outubro",
        "Novembro", "Dezembro"
    ];
    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();
    var min = date.getMinutes();
    var hour = date.getHours();
    if (min < 10) {
        min = '0' + min;
    }
    if (hour < 10) {
        hour = '0' + hour;
    }

    return "["+hour + ":" + min + "] " + day + ' de ' + monthNames[monthIndex] + ', ' + year;
}

Template.editarChamado.rendered = function () {
    $(document).ready(function () {
        $('#textCommit').summernote({
            placeholder: 'Adicione um comentário.',
            tabsize: 4,
            height: 90,
        });
    });
};

Template.editarChamado.helpers({
    modalChamado: function () {
        return getTicket();
    },

    equal: function (valChamado, val) {
        return valChamado === val;
    },

    telCliente: function(){
        let t = getTicket();
        let c = Clientes.findOne({nomeFantasia: t.cliente});
        return c.telefone_01;
    },

    tipoCliente: function(){
        let t = getTicket();
        let c = Clientes.findOne({nomeFantasia: t.cliente});
        return c.tipo;
    },

    getStatus: function(status) {
        switch (status) {
            case "novo" : 
                return '<span class="chamado-novo glyphicon glyphicon-flag" title="Novo Chamado" aria-hidden="true"></span>';
            case "atribuido" : 
                return '<span class="chamado-atribuido glyphicon glyphicon-flag" title="Chamado em andamento!" aria-hidden="true"></span>';
            case 'fechado' : //fechado
                return '<span class="chamado-fechado glyphicon glyphicon-flag" title="Chamado fechado" aria-hidden="true"></span>';
            case 'agendado' : //fechado
                return '<span class="chamado-agendado glyphicon glyphicon-flag" title="Chamado agendado" aria-hidden="true"></span>';
        }
    },

    isAtt: function(status){
        if(status === 'atribuido' || status === 'agendado'){
            return true;
        }else{return false}
    },

    tSetor: function() {
        let t = getTicket();
        switch(t.setor){
            case 'admin':
                return 'Administrativo';
            case 'lab':
                return 'Laboratório';
            case 'remoto':
                return 'Remoto';
            case 'externo':
                return 'Externo';
        }
    }
});

Template.editarChamado.events({
    'click #addCommit': function (evt) {
        evt.preventDefault();
        var text = $('#textCommit').summernote('code');
        if(text !== '<p><br></p>'){
            var commit = {
                usuario: Meteor.user().profile.nome,
                texto: text,
                hora: formatDate(new Date())
            }
            Meteor.call('updateTicket', Session.get('selectedTicketId'),commit);
            $('#textCommit').summernote('code', '<p><br></p>');
            swal(
                '',
                'Comentário feito com sucesso!',
                'success'
            )
        }else{
            swal(
                '',
                'Impossível realizar um comentário vazio!',
                'warning'
            )
        }
        
    },
    'click #closeTicket': function (evt) {
        evt.preventDefault();
        var id = this._id;
        swal({
            text: '',
            title: '<div class="cor">Deseja fechar o chamado?</div>',
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Sim, fechar',
            cancelButtonText: 'Não, manter status',
        }).then(function () {
            var text = $('#textCommit').summernote('code');
            if(text === '<p><br></p>'){                
                swal(
                    'Erro',
                    'Impossível fechar o chamado sem um breve comentário!',
                    'error'
                );
            }else{
                let usr = Meteor.user().profile.nome;
                let commit = {
                    usuario: usr,
                    texto: usr+' fechou o chamado. <br>'+text,
                    hora: formatDate(new Date())
                }
                Meteor.call('updateTicket', Session.get('selectedTicketId'),commit);
                Meteor.call('closeTicket', Session.get('selectedTicketId'));
                $('#textCommit').summernote('code', '<p><br></p>');  
                $('#showTicket').modal('hide');
                swal(
                    '',
                    'O chamado foi fechado!',
                    'success'
                );              
            } 
        }, function (dismiss) {
            swal(
                '',
                'O status do chamado foi mantido!',
                'info'
            )
        });
    },

    'click #btnAgendar': function(evt) {
        evt.preventDefault();
        var id = this._id;
        swal({
            text: '',
            title: '<div class="cor">Deseja agendar o chamado?</div>',
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Sim, agendar',
            cancelButtonText: 'Não, manter status',
        }).then(function () {
            var text = $('#textCommit').summernote('code');
            let setor = Session.get('perfil').setor;
            if(text === '<p><br></p>'){
                swal({
                    title: '<div class="cor">Deixar comentário em branco?</div>',
                    text: '',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Sim',
                    cancelButtonText: 'Não',
                }).then(function () {
                    let usr = Meteor.user().profile.nome;
                    let commit = {
                        usuario: usr,
                        texto: usr+' agendou o chamado.',
                        hora: formatDate(new Date())
                    }
                    Meteor.call('updateTicket', Session.get('selectedTicketId'),commit);
                    Meteor.call('agenda',Session.get('selectedTicketId'),usr,setor);
                    Meteor.call('alterar', Session.get('selectedTicketId'),'agendado');
                    $('#showTicket').modal('hide');
                    swal(
                        '',
                        'O chamado foi agendado!',
                        'success'
                    );
                }) 
            }else{
                let usr = Meteor.user().profile.nome;
                let commit = {
                    usuario: usr,
                    texto: usr+' agendou o chamado. <br>'+text,
                    hora: formatDate(new Date())
                }
                Meteor.call('updateTicket', Session.get('selectedTicketId'),commit);
                Meteor.call('agenda',Session.get('selectedTicketId'),usr,setor);
                Meteor.call('alterar', Session.get('selectedTicketId'),'agendado');
                $('#textCommit').summernote('code', '<p><br></p>');  
                $('#showTicket').modal('hide');
                swal(
                    '',
                    'O chamado foi agendado!',
                    'success'
                );              
            } 
        }, function (dismiss) {
            swal(
                '',
                'O status do chamado foi mantido!',
                'info'
            )
        });
    }

});