Template.regCliente.events({
    'click #editar': function (e) {
        e.preventDefault();
        ModalHelper.editarCliente(this._id);
    },

    'click #excluir': function (e) {
        var id = this._id
        
        swal({
          text: '',
          title: '<div class="cor">Deseja excluir o cliente?</div>',
          type: 'question',
          showCancelButton: true,
          confirmButtonText: 'Sim, deletar',
          cancelButtonText: 'Não, manter',
        }).then(function() {
          Meteor.call('rmClient', id, function (error, result) {
                if (error) {
                    alert(error);
                }else{
                    Bert.alert( 'Sucesso, cliente removido do sistema!', 'success', 'growl-top-right' );
                }
            });            
        }, function(dismiss) {
            Bert.alert( 'Cliente mantido no sistema!', 'info', 'growl-top-right' );            
        });        
        /*Meteor.call('rmClient', this._id, function (error, result) {
            if (error) {
                alert(error);
            }
        });*/
    },

    'click #detalhes': function (e) {
        e.preventDefault();
        ModalHelper.openModalFor(this._id);
    }

});


Template.regCliente.helpers({
    perfil: function () {
        return Session.get('perfil');
    },

    isAdm: function (setor) {
        return setor === 'admin';
    }
});
