var handle;
Template.forCliente.onCreated(function () {
    handle = Meteor.subscribeWithPagination('clientes',7);
});


Template.forCliente.helpers({
    clientes: function () {
        return Clientes.find();
    },

    perfil: function () {
        return Session.get('perfil');
    },

    isAdm: function (setor) {
        return setor === 'admin';
    }
});

Template.forCliente.events({
    'click #loadMore':function(){
        handle.loadNextPage();
        
        if(handle._loaded > Clientes.find().count()){
             Bert.alert( 'Não há mais registros de clientes!', 'default', 'fixed-bottom' );
            //$('.loadMore').remove();
        }
    }
});
