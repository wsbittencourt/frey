Template.addCliente.events({
    'click #btnCadastrar': function (evt) {
        evt.preventDefault();
        var nClient = {
            nomeFantasia: $('#nFantasia').val().toUpperCase(),
            responsavel: $('#responsavel').val().toUpperCase(),
            email: $('#email').val().toUpperCase(),
            telefone_01: $('#telefone_01').val(),
            tipo: $('#tipo').val(),
            ativo: $('#ativo').val()
        }
        Meteor.call('addClient', nClient, Meteor.user().username, function (error, result) {
            if (error) {
                alert(error);
            }
        });
        $('#formCliente').modal('hide');
    }
});