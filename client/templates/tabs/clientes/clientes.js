Session.setDefault('filtroCliente', false);
//Meteor.subscribe('searchClient');

Template.clientes.onCreated(function () {
  Session.set('search','');
});

Tracker.autorun(function() {
    if(Session.get('filtroCliente')){
        Meteor.subscribe('searchClient');
    }else{
        Meteor.subscribeWithPagination('clientes',7);
    }  
});

 // =============== Helper ==================

Template.clientes.helpers({
    'perfil': function () {
        return Session.get('perfil');
    },

    'isAdm': function (name) {
        return name === 'admin';
    },
    
    'statusFiltro': function () {
        return Session.get('filtroCliente');
    },
    
    filtro: function () {
        //var regexp = new RegExp(Session.get('search/keyword'), 'i');
        
        //return Clientes.find({nomeFantasia: regexp});        
        //return Meteor.call('filtroCliente',regexp,$('#ftipo').value,$('#fativo').value);
        var name = Session.get('search');
        Session.set('clienteTipo', $('#ftipo').val());
        Session.set('clienteAtivo', $('#fativo').val());
        if(name === ''){
            let ftipo = Session.get('clienteTipo');
            if(ftipo === 'Todos'){
                return Clientes.find({ativo: Session.get('clienteAtivo')});
            }else{
                return Clientes.find({
                    tipo: ftipo,
                    ativo: Session.get('clienteAtivo')
                }); 
            }
        }else{
            let ftipo = Session.get('clienteTipo');
            if(ftipo === 'Todos'){
                return Clientes.find({nomeFantasia: {$regex : ".*"+name+".*"}});
            }else{
                return Clientes.find({
                    nomeFantasia: {$regex : ".*"+name+".*"},
                    tipo: ftipo,
                    ativo: Session.get('clienteAtivo')
                }); 
            }
        }     
    }
});

//============

Template.clientes.events({
  'change #checker': function() {
    // Also, no need for the pound sign here
    if (document.getElementById('checker').checked)
      Session.set('filtroCliente', true);
    else
      Session.set('filtroCliente', false);
    },
    
    'keyup #filtroNomeFantasia': function(event) {
        Session.set('search', event.target.value.toUpperCase());
    },
    
    'click #refresh': function(event) {
        Session.set('clienteTipo', $('#ftipo').val());
        Session.set('clienteAtivo', $('#fativo').val());
    },

    'click #btnFiltrar': () => {
        if(Session.get('filtroCliente') === true){
            $('#checker').prop('checked', true);
        }        
    }
});

//===========


