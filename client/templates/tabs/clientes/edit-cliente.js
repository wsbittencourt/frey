getCliente = function(){
    var cId = Session.get('selectedClienteId');
    var c = Clientes.findOne(cId);
    return c;
}

Template.editCliente.helpers({
  modalCliente: function() {
      return getCliente();
  },
    
  equal: function(valCliente,val) {
    return valCliente === val;
  }
});

Template.editCliente.events({
    'click #btnEditar': function (evt) {
        evt.preventDefault();   
        var nClient = {
            nomeFantasia: $('#enFantasia').val().toUpperCase(),
            responsavel: $('#eresponsavel').val().toUpperCase(),
            email: $('#eemail').val().toUpperCase(),
            telefone_01: $('#etelefone_01').val(),
            tipo: $('#etipo').val(),
            ativo: $('#eativo').val()
        }   
        Meteor.call('updateClient', nClient, Session.get('selectedClienteId'), function (error, result) {
            if (error) {
                alert(error);
            }
        });         
        $('#eCliente').modal('hide');
        Bert.alert( 'Sucesso, os dados do cliente foram modificados!', 'success', 'growl-top-right' );
    }
});