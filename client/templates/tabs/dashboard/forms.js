getUsr = function () {
    let uId = Session.get('managerUsr');
    let u = Meteor.users.findOne(uId);
    return u;
}

Template.forms.onCreated(function () {
    Session.set('managerUsr', '');
    Session.set('prof', '');
    this.selectedUser = new ReactiveVar(false);
});

Template.forms.rendered = function () {
    $('#listaClientes').selectpicker();

}

Template.forms.helpers({
    isEmpty: () => {
        if (Session.get('managerUsr') === '') {
            return true;
        } else return false;
    },

    isClient: () => {
        let user = getUsr();
        try {
            Meteor.call('accProfile', user.username, function (err, res) {
                if (res) {
                    Session.set('prof', res);
                } else;//alert(err + '\nErro ao carregar perfil do usuário!');
            });
        } catch (e) { }
        let obj = Session.get('prof');
        if (typeof (obj.empresa) !== 'undefined') {
            return true;
        } else {
            return false;
        }
    },

    getUsr: () => {
        let u = getUsr();
        let prof = Session.get('prof');
        let usr = {
            username: u.username,
            name: u.profile.nome,
            email: u.emails[0].address
        };
        if (typeof (prof.empresa) !== 'undefined') {
            usr.empresa = prof.empresa;
        } else {
            usr.setor = prof.setor;

            Meteor.call('accProfile', usr.username, function (err, res) {
                if (res) {
                    Session.set('prof', res);
                } else;//alert(err + '\nErro ao carregar perfil do usuário!');
            });
            $('#colabSetor').val(Session.get('prof').setor);
        }
        let temp = Template.instance();
        temp.selectedUser.set(usr)
        return usr;
    },

    todosClientes: function () {
        Meteor.subscribe('searchClient');
        let query = Clientes.find({ ativo: 'Sim' });
        return query;
    },

    setor: () => {
        switch (Session.get('prof').setor) {
            case "admin": return 'Administração';
            case "lab": return 'Laboratório';
            case "remoto": return 'Remoto';
            case "externo": return 'Externo';
        }
    }

});

Template.forms.events({
    'click #resetPW': (event, template) => {
        swal({
            text: 'Deseja redefinir a senha do usuário?',
            title: '',
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Sim, redefinir',
            cancelButtonText: 'Não, manter senha',
        }).then(function () {
            let usuario = template.selectedUser.get();
            Meteor.call('resetPasswd', Meteor.user().username, usuario.username, (err, result) => {
                if (!err) {
                    swal(
                        '<span class="cor">Sucesso!</span>',
                        'Senha redefinida para: lN321',
                        'success'
                    );
                } else {
                    swal(
                        '<span class="cor">Erro</span>',
                        err.reason,
                        'error'
                    );
                }
            });
        }, function (dismiss) {
            swal(
                '',
                'Senha mantida no sistema!',
                'info'
            );
        });
    },

    'click #saveChanges': (event, template) => {
        let tmpUsr = template.selectedUser.get();

        if ($('#username').val() !== tmpUsr.username || $('#email').val() !== tmpUsr.email
            || $('#nome').val() !== tmpUsr.name || $('#listaClientes').val() !== null) {

            swal({
                text: '',
                title: '<div class="cor"><small>Deseja alterar informações do usuário?</small></div>',
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Sim, deletar',
                cancelButtonText: 'Não, manter',
            }).then(function () {

                let changes = {
                    username: $('#username').val(),
                    email: $('#email').val(),
                    nome: $('#nome').val(),
                }

                if ($('#listaClientes').val() !== null) {
                    if (typeof (tmpUsr.empresa) !== 'undefined') {
                        changes.empresa = $('#listaClientes').val();
                    } else {
                        changes.setor = $('#listaClientes').val();
                    }
                }

                Meteor.call('changes', Meteor.user().username, tmpUsr.username, changes, (err, result) => {
                    if (!err) {
                        swal(
                            '<span class="cor">Sucesso</span>',
                            'Todas as modificações foram salvas!',
                            'success'
                        );

                        Session.set('managerUsr', '');
                        Session.set('prof', '');
                    } else {
                        swal(
                            '<span class="cor">Erro</span>',
                            err.reason,
                            'error'
                        );
                    }
                });

            }, function (dismiss) {
                swal(
                    '',
                    'Informações atuais, mantidas no sistema!',
                    'info'
                );
            });

        } else {
            swal(
                '',
                'Não há modificações a serem salvas!',
                'info'
            );
        }
    },

    'click #btnClearForm': () =>{
        Session.set('managerUsr', '');
        Session.set('prof', '');
    }
});