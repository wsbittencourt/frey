Template.novoCliente.rendered = function () {
    $('#listaClientes').selectpicker();
};

Template.novoCliente.helpers({
    todosClientes: function () {
        Meteor.subscribe('searchClient');
        var query = Clientes.find({ ativo: 'Sim' });
        //Meteor.subscribeWithPagination('clientes',7);
        return query;
    }
});

Template.novoCliente.events({
    'click #btnNovoCliente': function () {
        if ($('#nome').val() === '' || $('#email').val() === '' ||
            $('#username').val() === '' || $('#password').val() === '' || 
            $('#listaClientes').val() === null) {
            swal(
                'Erro ao cadastrar usuário',
                'Preencha todos os campos para prosseguir!',
                'error'
            );
        } else {

            let usr = {
                username: $('#username').val(),
                password: $('#password').val(),
                email: $('#email').val(),
                nome: $('#nome').val(),
                empresa: $('#listaClientes').val()
            }

            $('#modalNcliente').modal('hide');
            Meteor.call('novoCliente', usr , (err) =>{
                if(err){
                    Bert.alert('Erro ao cadastrar usuário!', 'danger', 'growl-top-right');
                }
                else{
                    Bert.alert('Usuário cadastrado com sucesso!', 'success', 'growl-top-right');
                }
            });
        }
    }
});