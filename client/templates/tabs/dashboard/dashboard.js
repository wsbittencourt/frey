Template.index.events({
    'click #novoCliente': function (e) {
        e.preventDefault();
        ModalHelper.novoCliente();
    },
    'click #novoColaborador': function (e) {
        e.preventDefault();
        ModalHelper.novoColaborador();
    },
    'click #gerenciar': function (e) {
        e.preventDefault();
        ModalHelper.manager();
    }
});