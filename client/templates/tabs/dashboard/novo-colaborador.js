Template.novoColaborador.rendered = function () {
    $('#listaClientes').selectpicker();
};

Template.novoColaborador.helpers({
    todosClientes: function () {
        Meteor.subscribe('searchClient');
        var query = Clientes.find({ ativo: 'Sim' });
        return query;
    }
});

Template.novoColaborador.events({
    'change #admin': () => {
        if ($('#admin').is(':checked')) {
            $("#setor").attr("disabled", "disabled");
        }else{
            $('#setor').removeAttr("disabled");
        }
    },
    'change #checkUsr': () => {
        $('#setor').removeAttr("disabled");
    },

    'click #btnNovoColaborador': function () {
        
        if ($('#colabNome').val() === '' || $('#colabEmail').val() === '' ||
            $('#colabUsername').val() === '' || $('#colabPassword').val() === '') {
            swal(
                'Erro ao cadastrar colaborador',
                'Preencha todos os campos para prosseguir!',
                'error'
            );
        }else if(!$('#admin').is(':checked') && $('#setor').val() === null){
            swal(
                'Erro ao cadastrar colaborador',
                'Selecione um setor!',
                'error'
            );
        } else {
            let tp = 'user'
            let sec = $('#setor').val();

            if($('#admin').is(':checked')){
                tp = 'root';
                sec = 'admin';
            }

            let usr = {
                username: $('#colabUsername').val(),
                password: $('#colabPassword').val(),
                email: $('#colabEmail').val(),
                nome: $('#colabNome').val(),
                setor: sec,
                tipo: tp
            }

            $('#modalNcolaborador').modal('hide');
            Meteor.call('novoColaborador', usr , (err) =>{
                if(err){
                    Bert.alert('Erro ao cadastrar colaborador!', 'danger', 'growl-top-right');
                }
                else{
                    Bert.alert('Colaborador cadastrado com sucesso!', 'success', 'growl-top-right');
                }
            });
        }
    }
});