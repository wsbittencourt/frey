Template.user.events({
    'click #editUsr': function (e) {
        Session.set('managerUsr', this._id);
    },

    'click #deleteUsr': function () {
        let id = this._id;
        let username = this.username;
        swal({
          text: '',
          title: '<div class="cor"><small>Deseja excluir o usuário?</small></div>',
          type: 'question',
          showCancelButton: true,
          confirmButtonText: 'Sim, deletar',
          cancelButtonText: 'Não, manter',
        }).then(function() {
          Meteor.call('removeClient',Meteor.user().username, id, username, function (error, result) {
                if (error) {
                    alert(error);
                }else{
                    Session.set('managerUsr','');
                    swal(
                        '<span class="cor">Sucesso!</span>',
                        'Usuário excluído com sucesso',
                        'success'
                    );
                }
            });
        }, function(dismiss) {
            swal(
                '',
                'Usuário mantido no sistema!',
                'info'
            );           
        }); 
    }
});