Template.manager.onCreated(function () {
    Session.set('keyusr', '');
    Session.set('edUsr','');
    Meteor.subscribe('allUsers');    
});

Template.manager.helpers({
    all: function () {
        try {
            let chave = Session.get('keyusr');

            if (chave === '') {
                return Meteor.users.find({}, { sort: { createdAt: -1 } });
            } else {
                /*let username = Meteor.users.find({username: { $regex: ".*" + chave + ".*" }});                
                let nome = Meteor.users.find({"profile.nome": { $regex: new RegExp(chave)}});
                return username.concat(nome); */

                return Meteor.users.find({
                    $or: [{ username: { $regex: ".*" + chave + ".*" } },
                    { "profile.nome": { $regex: new RegExp(chave) } }]
                });
            }
        } catch (e) {
            //Faz nada
        }
    },
    selected: function () {
        return this.usr;
    }
});

Template.manager.events({
    'keyup #keyu': function (event) {
        Session.set('keyusr', event.target.value);
    }
});