Template.create.rendered = () => {
    $(document).ready(function () {
        $('#chamadoSummer').summernote({
            placeholder: 'Adicione uma descrição do problema',
            tabsize: 4,
            height: 200,
        });
    });
}

Template.create.onCreated(function () {
    Meteor.subscribe('client',Session.get('perfil').empresa);
    Meteor.call('getClient', Session.get('perfil').empresa, (error, result) => {
        if(error){
            console.log(error);
        }else{
            Session.set('tipo',result);
        }
    });
    console.log(Session.get('tipo').tipo);
});

Template.create.events({
    'click #btnCreate': function () {
        if ($('#chamadoSummer').summernote('code') === '<p><br></p>' ||
            $('#titulo').val() === '') {
            swal(
                'Erro ao abrir chamado',
                'Preencha todos os campos para prosseguir!',
                'error'
            );
        } else {
            try {
                let cId = Session.get('perfil').empresa;

                let nTicket = {
                    texto: $('#chamadoSummer').summernote('code'),
                    autor: Meteor.user().profile.nome.toUpperCase(),
                    cliente: cId,
                    titulo: $('#titulo').val().toUpperCase(),
                    createAt: '',
                    setor: 'remoto',
                    nOS: '',
                    status: '',
                    usuario: '',
                    criado: '',
                    tipo: Session.get('tipo').tipo,
                    responsavel: '',
                    open: true,
                    comentarios: new Array()
                }
                Meteor.call('addTicket', nTicket);
                $('#clienteChamado').modal('hide');

                //atualiza página
                //document.location.reload(true);

                Bert.alert('Chamado aberto com sucesso!', 'success', 'growl-top-right');
                $('#chamadoSummer').summernote('code', '<p><br></p>');
                $('#titulo').val('');
            } catch (e) { 
                alert(e);
            }
        }
    }
});