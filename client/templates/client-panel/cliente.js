//=====================================
$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() != 0) {
            $('#toTop').fadeIn();
        } else {
            $('#toTop').fadeOut();
        }
    });
});
//=====================================

Template.content.onCreated(function () {
    Session.set('chave', '');
    Session.set('open', true);
    Meteor.call('accProfile', Meteor.user().username, function (err, res) {
        if (res) {
            Session.set('perfil', res);
        } else alert('Erro ao carregar perfil do usuário!');
    });
    Meteor.subscribe('chamados');
});

Template.content.events({
    'click #toTop': function () {
        $('html, body').animate({ scrollTop: 0 }, 'slow');
    },
    'click #ch': function (e) {
        e.preventDefault();
        ModalHelper.changePasswd();
    }
});

Template.content.helpers({
    'perfil': function () {
        return Session.get('perfil');
    },
    'nome': function () {
        return Meteor.user().profile.nome;
    },
    'number': function () {
        try {
            let cnt = Tickets.find({ cliente: Session.get('perfil').empresa }).count();
            if (cnt === null) {
                return 0;
            }
            else {
                return cnt;
            }
        } catch (e) {
            //Faz nada
        }
    },
    'status': () => {
        return Session.get('open');
    },
    'qtd': (s) => {
        try {
            if (s === 'abertos') {
                return Tickets.find({ open: true, cliente: Session.get('perfil').empresa }).count();
            } else if (s === 'fechados') {
                return Tickets.find({ open: false, cliente: Session.get('perfil').empresa }).count();
            }
        } catch (e) {

        }
    }
});

Template.cliente.events({
    'click #sair': function (event) {
        event.preventDefault();

        // Clear all keys
        Object.keys(Session.keys).forEach(function (key) {
            Session.set(key, undefined);
        })
        Session.keys = {}

        Meteor.logout();
        Alerts.remove({});
        Router.go('login');
    },
    'keyup #chave': function (event) {
        Session.set('chave', event.target.value);
    },
    'click #open': function () {
        Session.set('open', true);
    },
    'click #close': function () {
        Session.set('open', false);
    },
    'click #create': function (e) {
        e.preventDefault();
        ModalHelper.create();
    }

});

Template.cregs.helpers({
    'chamados': function () {
        try {
            let chave = Session.get('chave');
            let prof = Session.get('perfil');
            let empresa = prof.empresa;
            let status = Session.get('open');

            if (chave === '') {
                return Tickets.find({ open: status, cliente: empresa });
            } else {
                let titulo = Tickets.find({ open: status, cliente: empresa, titulo: { $regex: ".*" + chave + ".*" } }).fetch();
                let conteudo = Tickets.find({ open: status, cliente: empresa, texto: { $regex: ".*" + chave + ".*" } }).fetch();
                return titulo.concat(conteudo);
            }
        } catch (e) {
            //Faz nada
        }
    },
    'situ': (val) => {
        if (val) {
            return 'Em aberto';
        } else {
            return 'Fechado';
        }
    },
    cmtFechamento: (arr) => {
        return arr[arr.length - 1];
    }
});