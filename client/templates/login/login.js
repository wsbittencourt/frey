Template.login.events({
    'submit form': function (event) {
        event.preventDefault();
        Meteor.loginWithPassword($('#uID').val(),
            $('#pwd').val(),
            function (err) {
                if (err) {
                    if ($('#uID').val().match(/ /)) {
                        //return throwError('Remova todos os espaços do ID');
                        return Bert.alert( 'Remova todos os espaços do ID', 'danger');
                    } else {
                        return Bert.alert( 'Erro ID ou senha, inválidos!', 'danger');
                        //return throwError('Erro ID ou senha, inválidos!');
                    }
                } else {
                    let nome = Meteor.user().profile.nome;
                    Meteor.call('accProfile', Meteor.user().username, function (err, res) {
                        if (res) {
                            Session.set('perfil', res);
                            if(res.tipo === 'client'){
                                Router.go('cliente');
                                Bert.alert({
                                    type: 'info',
                                    style: 'growl-top-right',
                                    title: 'Olá!',
                                    message: 'Seja bem-vindo ao LN panel',
                                    icon: 'fa-bell'
                                  });
                            } else{
                                //newAlert("Bem-vindo "+nome+"!"); 
                            Router.go('panel');
                            Bert.alert({
                                type: 'info',
                                style: 'growl-top-right',
                                title: 'Bem-vindo '+nome+'!',
                                message: 'Essa é uma versão de teste. Desculpe quaisquer inconveniências.',
                                icon: 'fa-bell'
                              });
                            } 
                        } else alert('Erro ao carregar perfil do usuário!');
                    });

                    /*
                        //newAlert("Bem-vindo "+nome+"!"); 
                    Router.go('panel');
                    Bert.alert( "Bem-vindo "+nome+"! Essa é uma versão de teste. Desculpe quaisquer inconveniências ", 'info');
                    */                   
                }
            });
    }
});
