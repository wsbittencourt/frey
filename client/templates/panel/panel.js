(function ($) {
    "use strict";
    var mainApp = {

        main_fun: function () {

            /*====================================
              LOAD APPROPRIATE MENU BAR
           ======================================*/
            $(window).bind("load resize", function () {
                if ($(this).width() < 768) {
                    $('div.sidebar-collapse').addClass('collapse')
                } else {
                    $('div.sidebar-collapse').removeClass('collapse')
                }
            });



        },

        initialization: function () {
            mainApp.main_fun();

        }

    }
    // Initializing ///

    $(document).ready(function () {
        mainApp.main_fun();
    });

}(jQuery));

//============================

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    try{
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("myBtn").style.display = "block";
        } else {
            document.getElementById("myBtn").style.display = "none";
        }
    }catch(e){
        //Faz nada
    }
}

//==========================

Template.index.events({
    'click .sair': function (event) {
        event.preventDefault();
        // Reset one value
        /*Session.set('perfil', undefined);
        delete Session.keys['perfil'];*/

        // Clear all keys
        Object.keys(Session.keys).forEach(function (key) {
            Session.set(key, undefined);
        })
        Session.keys = {}

        Meteor.logout();
        Alerts.remove({});
        Router.go('login');
    },

    'click #change': function (e) {
        e.preventDefault();
        ModalHelper.changePasswd();
    },

    'click #main-menu li': function (event, template) {
        let currentTab = $(event.target).closest("li");

        currentTab.addClass("active");
        $("#main-menu li").not(currentTab).removeClass("active");
        template.currentTab.set(currentTab.data("template"));
    },
    
    'click #myBtn': function topFunction() {// When the user clicks on the button, scroll to the top of the document
        $('html, body').animate({scrollTop:0}, 'slow');
    },

    'click #btnClientes': function() {
        swal({
            title: '<div class="cor">Carregando...</div>',
            //text: 'Aguarde...',
            type: 'info',
            timer: 2500,
            showConfirmButton: false,
            onOpen: function () {
              swal.showLoading()
            }
          })

          $('html, body').animate({scrollTop:0}, 'slow');
    },

    'click #btnChamados': function() {
        $('html, body').animate({scrollTop:0}, 'slow');
    },

    'click #btnDash': function() {
        $('html, body').animate({scrollTop:0}, 'slow');
    },

    'click #home': () => {
        location.reload();
    }
});


Template.index.helpers({
    'perfil': function () {
        return Session.get('perfil');
    },

    'isAdmin': function (name) {
        return name === 'admin';
    },

    tab: function () {
        return Template.instance().currentTab.get();
    }
});

Template.index.onCreated(function () {
    let perfil = Session.get('perfil');
    
    Meteor.call('accProfile', Meteor.user().username, function (err, res) {
        if (res) {
            Session.set('perfil', res);
            if(res.tipo === 'client'){
                Router.go('cliente');
            } 
        } else alert('Erro ao carregar perfil do usuário!');
    });    
    this.currentTab = new ReactiveVar("chamados");  
});