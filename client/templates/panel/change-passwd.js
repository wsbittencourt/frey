Template.changePasswd.events({
    'click #btnChange': function (event) {
        if ($('#atual').val() === '' || $('#nova').val() === '' ||
        $('#confNova').val() === '' ) {
            swal(
                'Erro',
                'Preencha todos os campos para prosseguir!',
                'error'
            );
        } else {

            if ($('#nova').val() !== $('#confNova').val()) {
                swal(
                    'Erro',
                    'A confirmação está diferente da nova senha!',
                    'error'
                );
                $('#atual').val('');
                $('#nova').val('');
                $('#confNova').val('');
            }else{
                swal({
                    text: '',
                    title: '<div class="cor">Deseja salvar alterarção?</div>',
                    type: 'question',
                    showCancelButton: true,
                    confirmButtonText: 'Sim, salvar',
                    cancelButtonText: 'Não, descartar alterações',
                }).then(function () {
                    Accounts.changePassword($('#atual').val(), $('#nova').val(), function(err){
                        if(err) {
                            $('#squarespaceModal').modal('hide');
                            $('#nova').val('');
                            $('#confNova').val('');

                            if( err.message === 'Incorrect password [403]'){
                                swal({
                                    title:'Erro',
                                    html:"<br><span class='cor'>ERRO: Campo <strong>Senha atual</strong>, não corresponde ao valor esperado.</span>",
                                    type:'error'
                                });
                            }else{
                                swal({
                                    title:'Erro',
                                    html:"Ocorreu um erro ao alterar a senha!",
                                    type:'error'
                                });
                            }
                        } else{
                            $('#squarespaceModal').modal('hide');
                            $('#atual').val('');
                            $('#nova').val('');
                            $('#confNova').val('');
                            swal(
                                'Sucesso',
                                'Alteração de senha bem sucedida!',
                                'success'
                            ); 
                        }
                    });

                }, function (dismiss) {
                    $('#squarespaceModal').modal('hide');
                    $('#atual').val('');
                    $('#nova').val('');
                    $('#confNova').val('');
                    swal(
                        '',
                        'A senha atual foi mantida!',
                        'info'
                    )
                });
            }
        }
    }
});