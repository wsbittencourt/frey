buscar = function (status, f) {
    let resultado = new Array();
    let rTipo;
    let rTitulo;
    let rUsuario;
    let rCreateAt;
    let rSetor;
    let rCliente;
    if(status === 'emAberto'){
        rTipo = Tickets.find({open:true,tipo: f.tipo});
        rTitulo = Tickets.find({open:true,titulo: f.titulo});
        rUsuario = Tickets.find({open:true,usuario: f.pessoa});
        rCreateAt = Tickets.find({open:true,createAt: f.createAt});
        rSetor = Tickets.find({open:true,setor: f.setor});
        rCliente = Tickets.find({open:true,cliente: f.cliente});
    }else if(status === 'fechados'){
        rTipo = Tickets.find({open:false,tipo: f.tipo});
        rTitulo = Tickets.find({open:false,titulo: f.titulo});
        rUsuario = Tickets.find({open:false,usuario: f.pessoa});
        rCreateAt = Tickets.find({open:false,createAt: f.createAt});
        rSetor = Tickets.find({open:false,setor: f.setor});
        rCliente = Tickets.find({open:false,cliente: f.cliente});
    }else if(status === 'todos'){
        rTipo = Tickets.find({tipo: f.tipo});
        rTitulo = Tickets.find({titulo: f.titulo});
        rUsuario = Tickets.find({usuario: f.pessoa});
        rCreateAt = Tickets.find({createAt: f.createAt});
        rSetor = Tickets.find({setor: f.setor});
        rCliente = Tickets.find({cliente: f.cliente});
    }
    
    
    if(rTipo.count() !== 0){
        rTipo.forEach(
            function(item) {
                if(!resultado.includes(item)){
                    resultado.push(item);
                }               
            }
        );
    }
    
    if(rTitulo.count() !== 0){
        rTitulo.forEach(
            function(item) {
                if(!resultado.includes(item)){
                    resultado.push(item);}
            }
        );
    }
    if(rUsuario.count() !== 0){
        rUsuario.forEach(
            function(item) {
                if(!resultado.includes(item)){
                    resultado.push(item);}
            }
        );
    }
    if(rCreateAt.count() !== 0){
        rCreateAt.forEach(
            function(item) {
                if(!resultado.includes(item)){
                    resultado.push(item);}
            }
        );
    }
    if(rSetor.count() !== 0){
        rSetor.forEach(
            function(item) {
                if(!resultado.includes(item)){
                    resultado.push(item);}
            }
        );
    }
    if(rCliente.count() !== 0){
        rCliente.forEach(
            function(item) {
                if(!resultado.includes(item)){
                    resultado.push(item);}
            }
        );
    }
    
    return resultado;
}