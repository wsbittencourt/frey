/* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
openNav = () => {
    if ($(window).width() > 768) {
        document.getElementById("mySidenav").style.width = "300px";
        document.getElementById("main").style.marginLeft = "300px";
    } else {
        document.getElementById("mySidenav").style.width = "100%";
        document.getElementById("main").style.marginLeft = "1px";
    }

    document.getElementById("btnFiltrar").style.display = "none";
    document.getElementById("btnFiltrar").style.visibility = "none";
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
closeNav = () => {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
    document.getElementById("btnFiltrar").style.display = "block";
}