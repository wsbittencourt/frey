ModalHelper = {};

ModalHelper.openModalFor = function(clienteId) {
  Session.set('selectedClienteId', clienteId);
  Modal.show('showCliente');
}

ModalHelper.editarCliente = function(clienteId) {
  Session.set('selectedClienteId', clienteId);
  Modal.show('editCliente');
}

ModalHelper.editarChamado = function(ticketId) {
  Session.set('selectedTicketId', ticketId);
  Modal.show('editarChamado');
}

ModalHelper.moverChamado = function(ticketId) {
  Session.set('selectedTicketId', ticketId);
  Modal.show('moverChamado');
}

ModalHelper.changePasswd = function() {
  //Session.set('selectedTicketId', ticketId);
  Modal.show('changePasswd');
}

ModalHelper.create= function() {
  Modal.show('create');
}

ModalHelper.novoCliente = function() {
  Modal.show('novoCliente');
}

ModalHelper.novoColaborador = function() {
  Modal.show('novoColaborador');
}

ModalHelper.manager = function() {
  Modal.show('manager');
}